﻿using NFluent;
using NUnit.Framework;
using System;

using MyDiamond = Diamond.Console.Diamond;

namespace DiamondTests
{
    public class DiamondTests
    {
        [Test]
        public void Should_display_a_one_row_diamond_When_A()
        {
            var input = 'A';
            var output = MyDiamond.Display(input);

            Check.That(output).IsEqualTo("A");
        }

        [Test]
        public void Should_display_a_three_rows_diamond_When_B()
        {
            var input = 'B';
            var output = MyDiamond.Display(input);

            Check.That(output).IsEqualTo(
" A " + Environment.NewLine +
"B B" + Environment.NewLine +
" A ");
        }

        [Test]
        public void Should_display_a_five_rows_diamond_When_C()
        {
            var input = 'C';
            var output = MyDiamond.Display(input);

            Check.That(output).IsEqualTo(
"  A  " + Environment.NewLine +
" B B " + Environment.NewLine +
"C   C" + Environment.NewLine +
" B B " + Environment.NewLine +
"  A  ");
        }

        [Test]
        public void Should_display_a_seven_rows_diamond_When_D()
        {
            var input = 'D';
            var output = MyDiamond.Display(input);

            Check.That(output).IsEqualTo(
"   A   " + Environment.NewLine +
"  B B  " + Environment.NewLine +
" C   C " + Environment.NewLine +
"D     D" + Environment.NewLine +
" C   C " + Environment.NewLine +
"  B B  " + Environment.NewLine +
"   A   ");
        }


        [TestCase('A', 'A', "A")]
        [TestCase('A', 'B', " A")]
        [TestCase('B', 'B', "B ")]
        [TestCase('C', 'C', "C  ")]
        [TestCase('B', 'C', " B ")]
        public void Should_display_a_half_row_corresponding_to_a_letter(char letter, char initialLetter, string expectedRow)
        {
            var actualRow = MyDiamond.DisplayHalfRow(letter, initialLetter);
            Check.That(actualRow).IsEqualTo(expectedRow);
        }

        [TestCase("A", "A")]
        [TestCase(" B ", " B B ")]
        [TestCase("C  ", "C   C")]
        public void Should_complete_a_half_row_by_applying_a_symetry(string halfRow, string expectedRow)
        {
            var actualRow = MyDiamond.CompleteHalfRow(halfRow);
            Check.That(actualRow).IsEqualTo(expectedRow);
        }
    }
}
