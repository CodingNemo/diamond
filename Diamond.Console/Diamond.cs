﻿using System;
using System.Collections.Generic;

namespace Diamond.Console
{
    public class Diamond
    {
        public static string Display(char inputLetter)
        {
            var halfDiamond = BuildHalfDiamond(inputLetter);
            var diamond = CompleteHalfDiamond(halfDiamond);
            return string.Join(Environment.NewLine, diamond);
        }

        private static List<string> BuildHalfDiamond(char inputLetter)
        {
            var halfDiamond = new List<string>();

            var spreadToA = inputLetter - 'A';

            for (int spread = 0; spread <= spreadToA; spread++)
            {
                char currentLetter = (char)('A' + spread);
                var halfrow = DisplayHalfRow(currentLetter, inputLetter);
                var row = CompleteHalfRow(halfrow);
                halfDiamond.Add(row);
            }

            return halfDiamond;
        }

        private static List<string> CompleteHalfDiamond(List<string> halfDiamond)
        {
            var fullDiamond = new List<string>();
            fullDiamond.AddRange(halfDiamond);

            var otherHalf = new List<string>();
            for(int lineIndex = 0; lineIndex < halfDiamond.Count -1; lineIndex++)
            {
                var line = halfDiamond[lineIndex];
                otherHalf.Add(line);
            }

            otherHalf.Reverse();
            fullDiamond.AddRange(otherHalf);

            return fullDiamond;
        }

        public static string DisplayHalfRow(char currentLetter, char initialLetter)
        {
            var outsidePadding = initialLetter - currentLetter;
            var insidePadding = currentLetter - 'A';

            return currentLetter.ToString()
                         .PadLeft(outsidePadding + 1)
                         .PadRight(outsidePadding + insidePadding + 1);
        }

        public static string CompleteHalfRow(string halfRow)
        {
            string otherHalf = string.Empty;

            for (int index = 0; index < halfRow.Length - 1; index++)
            {
                otherHalf = halfRow[index] + otherHalf;
            }

            return halfRow + otherHalf;
        }
    }
}